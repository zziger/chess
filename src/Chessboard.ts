import {BoxGeometry, Mesh, MeshLambertMaterial, Scene, Vector3} from "three";

export default class Chessboard {
    private static readonly geometry = new BoxGeometry(1, 1, 1);
    private static readonly whiteMaterial = new MeshLambertMaterial({ color: 0xffffff });
    private static readonly blackMaterial = new MeshLambertMaterial({ color: 0x000000 });
    private static readonly brownMaterial = new MeshLambertMaterial({ color: 0x7b3513 });

    private static readonly size = 1;

    private cells: Record<string, Mesh> = {};

    generate() {
        for (let i = 65; i <= 72; i++) {
            for (let j = 1; j <= 8; j++) {
                const object = new Mesh(Chessboard.geometry, [
                    Chessboard.brownMaterial,
                    Chessboard.brownMaterial,
                    ((i+j) % 2 === 0) ? Chessboard.whiteMaterial : Chessboard.blackMaterial,
                    Chessboard.brownMaterial,
                    Chessboard.brownMaterial,
                    Chessboard.brownMaterial,
                ]);
                object.scale.set(Chessboard.size, Chessboard.size, Chessboard.size);
                object.position.set(i-65 - (7 * Chessboard.size / 2), 0, j-1 - (7 * Chessboard.size / 2));
                this.cells[String.fromCharCode(i) + j] = object;
                this.scene.add(object);
            }
        }
    }

    constructor(private scene: Scene) {
        this.generate();
    }
}