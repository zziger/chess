import {
    AmbientLight,
    DirectionalLight,
    Light,
    PerspectiveCamera,
    Scene,
    WebGLRenderer
} from "three";
import * as three from 'three';
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
import Chessboard from "./Chessboard";

class ControlsScene {
    private static readonly defaultFov = 45;

    private readonly renderer: WebGLRenderer;
    // private readonly effect: AsciiEffect;
    private readonly camera: PerspectiveCamera;
    private readonly scene: Scene;
    private readonly controls: OrbitControls;
    private readonly ambientLight: Light;
    private readonly light: Light;

    private chessboard: Chessboard;

    get aspect(): number {
        return window.innerWidth / window.innerHeight;
    }

    constructor() {
        this.renderer = new WebGLRenderer({ alpha: true, antialias: true });

        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.renderer.setClearColor(0x000000, 0xff);


        // this.effect = new AsciiEffect( this.renderer, ' .:-+*=%@#', { invert: true } );
        // this.effect.setSize( window.innerWidth, window.innerHeight );
        // this.effect.domElement.style.color = 'white';
        // this.effect.domElement.style.backgroundColor = 'black';

        this.camera = new PerspectiveCamera(ControlsScene.defaultFov, this.aspect, 0.01, 30000);
        this.camera.position.set(0, 8, 15);
        this.camera.lookAt(0, 0, 0);

        this.controls = new OrbitControls(this.camera, this.renderer.domElement);
        this.controls.rotateSpeed = 0.5;
        this.controls.minDistance = 10;
        this.controls.maxDistance = 20;
        this.controls.enableZoom = true;
        this.controls.enablePan = false;
        this.controls.update();
        this.controls.target.set(0, 2, 0);

        this.scene = new Scene();
        // this.scene.add(new GridHelper(10, 10, 0x888888, 0x444444));

        this.ambientLight = new AmbientLight(0xffffff, 0.1);
        this.scene.add(this.ambientLight);

        this.light = new DirectionalLight(0xffffff, 0.5);
        this.light.position.set(0, 5, 5);
        this.light.lookAt(0, 0, 0);
        this.scene.add(this.light);

        this.chessboard = new Chessboard(this.scene);

        document.body.appendChild(this.renderer.domElement);

        window.addEventListener('resize', this.onResize);

        this.render();
    }

    render = (): void => {
        this.renderer.render(this.scene, this.camera);
        this.controls.update();
        requestAnimationFrame(this.render)
    };

    onResize = (): void => {
        this.camera.aspect = this.aspect;
        this.camera.updateProjectionMatrix();

        this.renderer.setSize( window.innerWidth, window.innerHeight );
    }
}

(window as any).scene = new ControlsScene();
(window as any).three = three;