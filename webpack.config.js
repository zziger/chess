const HtmlWebPackPlugin = require('html-webpack-plugin');
const path = require("path");

const htmlPlugin = new HtmlWebPackPlugin({
    template: './public/index.html',
    filename: './index.html',
});

module.exports = {
    mode: 'development',
    devtool: "eval-cheap-source-map",

    resolve: {
        extensions: ['.ts', '.js'],
    },

    entry: path.resolve(__dirname, 'src/index.ts'),
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: 'bundle.js',
        publicPath: './',
    },

    module: {
        rules: [
            {
                test: /\.ts$/,
                use: [
                    { loader: 'awesome-typescript-loader' },
                ],
                exclude: /node_modules/
            },
        ]
    },

    plugins: [
        htmlPlugin
    ]
}